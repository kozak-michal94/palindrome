/*
 * Front-end logic
 */
(function(angular) {

  angular.module('QuestionModule', []);

  angular.module('QuestionModule').controller('QuestionController', ['$scope', '$http', function($scope, $http) {
    $scope.question = {
      isLoading: false,
      lastResult: null,
      results: []
    };

    $scope.submitPalindrom = function() {
      $scope.question.isLoading = true;
      let isPal;
      $http.get('/palindrome/' + encodeURI($scope.question.value))
        .then(function success() {
          isPal = true;
        })
        .catch(function error(e) {
          isPal = false;
        })
        .finally(function(e) {
            $scope.question.results.unshift({
                isPal: isPal,
                value: $scope.question.value
            })
        });
    };

  }]);


  angular.module("Palindrome", ['ngRoute', 'QuestionModule'])
    .controller("RouterController", ['$scope', function($scope) {

    }])
    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.
      when('/', {
        templateUrl: 'static/partials/index.html'
      }).
      when('/palindrome', {
        templateUrl: 'static/partials/question.html'
      }).
      otherwise({
        redirectTo: '/'
      });
    }]);


})(angular);
