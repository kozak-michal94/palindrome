/**
 * Route Mappings
 * (sails.config.routes)
 */

module.exports.routes = {

  'GET /palindrome/:value': 'PaliController.check',

  'GET /': {
      view: 'page'
  },
  'GET /palindrome': {
      view: 'page'
  }

};
