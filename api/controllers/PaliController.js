'use strict';
/**
 * PaliController
 *
 * @description :: Logic for palindrome
 */

/**
 * Required ES6
 *
 * @param  {String}  bid Guessed palindrome
 * @return {Boolean}     Is palindrome or not
 */
var isPalindrome = bid => new RegExp([...bid].reverse().join``, 'i').test(bid);

module.exports = {
  /**
   * `Check if received word is a palindrome
   */
  check: function(req, res) {
	let bid = req.param('value'),
        isPal = bid && isPalindrome(bid);
    res.status(isPal ? 200 : 400).json({
      result: isPal,
      value: bid
    });
  }
};
